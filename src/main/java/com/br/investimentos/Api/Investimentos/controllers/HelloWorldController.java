package com.br.investimentos.Api.Investimentos.controllers;

import com.br.investimentos.Api.Investimentos.services.InstanceInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    @Autowired
    private InstanceInformationService service;
    @GetMapping(path = "/hello-world")
    public String helloWorld() {
        return "Hello World " + service.retrieveInstanceInfo();
    }
}
